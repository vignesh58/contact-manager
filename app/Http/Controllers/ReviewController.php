<?php

namespace App\Http\Controllers;
use App\Models\Review_user;
use App\Models\Review;
use App\Models\Contact;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReviewSuccessUserMail;
use App\Mail\ReviewSuccessAdminMail;

use Illuminate\Http\Request;

class ReviewController extends Controller
{

  public function review_form($id){
    $encoded_contact_id = base64_decode($id);
    
    $Contact_details  = Contact::where("id", $encoded_contact_id)->first();
      // dd($Contact_details);
    if($Contact_details != null){
      return view('reviews.reviews_form',compact("encoded_contact_id"));
    }else{
      return redirect()->back()->with('message', 'Invalid URL!!!'); 
    }
  }

  public function review_add(Request $request){
    $this->validate($request, [
      'first_name' => 'required',
      'last_name' => 'required',
      'email' => 'required|email',
      'university' => 'required',
      'review' => 'required']);
    $is_Valid_Email = $this->validateEmail('review',$request->email);
    if($is_Valid_Email){
     $review_entry = new Review_user;
     $review_entry->first_name = $request->first_name;
     $review_entry->last_name = $request->last_name;
     $review_entry->email = $request->email;
     $review_entry->university = $request->university;
     $review_entry->is_student = $request->is_student;
     $review_entry->save();

     $last_insert_id = $review_entry->id;
     $review_user_entry = new Review;
     $review_user_entry->review_user_id =  $last_insert_id;
     $review_user_entry->review = $request->review;
     $review_user_entry->contact_id = $request->contact_id;
     $review_user_entry->save();

     Mail::to($review_entry->email)->send(new ReviewSuccessUserMail($review_entry));
     Mail::to("vignesh@tulaa.in")->send(new ReviewSuccessAdminMail($review_entry->email));


    return redirect()->back()->with('message', 'Review Submited Sucessfully!!!');

}
   
   else{

     $review_user_update = Review_user::where("email", $request->email)->update(["first_name" => $request->first_name,"last_name" => $request->last_name,"university" => $request->university,"is_student" => $request->is_student]);

     $review_user  = Review_user::where("email", $request->email)->first();

     $review_user_entry = new Review;
     $review_user_entry->review_user_id =  $review_user->id;
     $review_user_entry->review = $request->review;
     $review_user_entry->contact_id = $request->contact_id;
     $review_user_entry->save();

     Mail::to($review_user->email)->send(new ReviewSuccessUserMail($review_user));
     Mail::to("vignesh@tulaa.in")->send(new ReviewSuccessAdminMail($review_user->email));


     return redirect()->back()->with('message', 'Review Submited Sucessfully!!!'); 
   }

 }

 public function review_list(){
  $review_data = Review_user::with('review.contact')->get()->toArray();
  
  return view('reviews.reviews_list',compact("review_data"));

}
}
