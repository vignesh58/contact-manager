<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Contact;


class ContactController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(){
   $data = Contact::all();
   return view('contact.contact_list',compact('data'));
 }

 public function contact_create(){

   $data['action'] = 'add';
   $data['form_action'] = 'contact-entry';
   $data['title'] = "Create Contact";
    	// dd($data);
   return view('contact.contact_create',compact("data"));;
 }
 public function contact_add(Request $request){
  $this->validate($request, [
    'first_name' => 'required',
    'last_name' => 'required',
    'email' => 'required|email',
    'phn_no' => 'required|numeric|digits:10',
    'address' => 'required',
  ],['phn_no.required'=>"Phone number is requied !",
  'phn_no.numeric'=>"Phone number must be in numeric !",
  'phn_no.digits'=>"Phone number size must be 10 !"
]);
  $is_Valid_Email = $this->validateEmail('create',$request->get('email'));
  if($is_Valid_Email){
   $contact_entry = new Contact;
   $contact_entry->first_name = $request->first_name;
   $contact_entry->last_name = $request->last_name;
   $contact_entry->email = $request->email;
   $contact_entry->phone = $request->phn_no;
   $contact_entry->address = $request->address;
   $contact_entry->save();
   return redirect()->route('contact-list')->with('message', 'Contact saved Sucessfully!!!');
 }
 else{
    // dd("error");
   return redirect()->back()->with('error', 'Email already exist!'); 
 }

}
public function contact_edit($id){

 $data['action'] = 'edit';
 $data['form_action'] = 'contact-update';
 $data['title'] = "Edit Contact";
 $contact_data = Contact::where('id',$id)->first()->toArray();
 return view('contact.contact_create',compact("data","contact_data"));;

}
public function contact_update(Request $request){

 $this->validate($request, [
  'first_name' => 'required',
  'last_name' => 'required',
  'email' => 'required|email',
  'phn_no' => 'required|numeric|digits:10',
  'address' => 'required',
],
['phn_no.required'=>"Phone number is requied !",
'phn_no.numeric'=>"Phone number must be in numeric !",
'phn_no.digits'=>"Phone number size must be 10 !"
]);

 $is_Valid_Email = $this->validateEmail('update',$request->get('email'),$request->edit_id);
 if($is_Valid_Email){
   $Contact_Update = Contact::where("id", $request->edit_id)->update(["first_name" => $request->first_name,"last_name" => $request->last_name,"email" => $request->email,"phone" => $request->phn_no,"address" => $request->address]);

   return redirect()->route('contact-list')->with('message', 'Contact Updated Sucessfully!!!');
 }else{
  return redirect()->back()->with('error', 'Email already exist!');
}

}
public function contact_delete($id){

 $Contact_Update = Contact::where("id", $id)->delete();

 return redirect()->route('contact-list')->with('message', 'Contact Deleted Sucessfully!!!');
}

}
