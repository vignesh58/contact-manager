<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Contact;
use App\Models\Review_user;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     public function validateEmail($type,$email,$user_id=false)
    {
        if($type=='update'){
               $user_exists =  Contact::where('id', '<>', $user_id)
                ->where('email', $email)
                ->first();
        }else if($type=='create'){
            $user_exists =  Contact::where('email',$email)->first();
        }
        else if($type=='review'){
            $user_exists = Review_user::where('email',$email)->first();
        }
        if ($user_exists) {
            return false;
        }else{
            return true;
        }
    }
    }

