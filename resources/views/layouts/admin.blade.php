<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="_token" content="{{ csrf_token() }}" />
  <title>Contact Management | Dashboard</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/custom_style.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<!-- jQuery -->
<script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js" ></script>

<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->


<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
 -->

<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('js/engagement_details.js') }}"></script>
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<style>
 .pointer{cursor:pointer;}
  #b1{  background-color:#d8d3d3;  }
  #b2{  background-color:#d8d3d3;  }
  #b3{  background-color:#d8d3d3;  }

  #b1:hover{  background-color:green;  }
  #b2:hover{  background-color:red;  }
  #b3:hover{  background-color:#ffc107;  }

  #b1.active{  background:green!important;  }
  #b2.active{  background:red!important;  }
  #b3.active{  background:#ffc107!important;  }
  #parent_btn_1{  background-color:#d8d3d3;  }
  #parent_btn_1:hover{  background-color:green;  }
  #parent_btn_2{  background-color:#ff0000 !important;  border-color:#ff0000 !important;  }
  #subsidiary_btn_1{  background-color:#d8d3d3 !important;  color:black !important;  }
  #subsidiary_btn_1:hover{  background-color:green !important;  }
  #subsidiary_btn_2{
    background-color:#ff0000 !important;
    border-color:#ff0000 !important;
  }
  #associative_btn_1{
    background-color:#d8d3d3 !important;
    color:black !important;
  }
  #associative_btn_1:hover{
       background-color:green !important;
  }
   #associative_btn_2{
    background-color:#ff0000 !important;
    border-color:#ff0000 !important;
  }
  #audit_completion_letter_btn_1:hover{
     background-color:green;
  }
  #audit_completion_letter_btn_2{
      background-color:#d8d3d3;
      color:black;
  }
  #audit_completion_letter_btn_2:hover{
     background-color:#ff0000!important;
     border-color:#ff0000 !important;
  }
  #audit_completion_letter_btn_2:active{
     background-color:#ff0000;
     background-color:#ff0000 !important;
  }
  #audit_completion_letter_btn_1{
    background-color:#d8d3d3;
    color:black;
  }
  #audit_completion_letter_btn_1:hover{
       background-color:green !important;
  }
  #audit_completion_letter_btn_1:hover{
     background-color:green;
  }
  #audit-completion-letter > .active{
    background-color:#000000 !important;
    color:black !important;
  }
  /************** NOC start ********************/
  #noc_letter_btn_3:hover{
     background-color:green;
  }
  #noc_letter_btn_4{
      background-color:#d8d3d3;
      color:black;
  }
  #noc_letter_btn_4:hover{
     background-color:#ff0000!important;
     border-color:#ff0000 !important;
  }
  #noc_letter_btn_4:active{
     background-color:#ff0000;
     background-color:#ff0000 !important;
  }
  #noc_letter_btn_3{
    background-color:#d8d3d3;
    color:black;
  }
  #noc_letter_btn_3:hover{
       background-color:green !important;
  }
  #noc_letter_btn_3:hover{
     background-color:green;
  }
  #noc-letter > .active{
    background-color:#000000 !important;
    color:black !important;
  }
/************** NOC end ********************/
  .yesactive{
    background-color:green !important;
  }
  .noactive{
    background-color:#ff0000 !important;
  }
.fa-trash
{
  color:white;
}

.accordion__header {
	padding: 10px;
	background-color: #ccc;
	margin-top: 2px;
	display: flex;
	justify-content: space-between;
	align-items: center;
	cursor: pointer;
}
.accordion__header > * {
	margin-top: 0;
	margin-bottom: 0;
	font-size: 16px;
}
.accordion__header.is-active {
	background-color: #343a40;
	color: #fff;
}

.accordion__toggle {
	margin-left: 10px;
	height: 3px;
	background-color: #222;
	width: 13px;
	display: block;
	position: relative;
	flex-shrink: 1;
	border-radius: 2px;
}

.accordion__toggle::before {
	content: "";
	width: 3px;
	height: 13px;
	display: block;
	background-color: #222;
	position: absolute;
	top: -5px;
	left: 5px;
	border-radius: 2px;
}

.is-active .accordion__toggle {
	background-color: #fff;
}
.is-active .accordion__toggle::before {
	display: none;
}


.accordion__body {
	display: none;
	padding: 10px;
	border: 1px solid #ccc;
	border-top: 0;
}
.accordion__body.is-active {
	display: block;
}
.info p small{
  color:#c2c7d0;
  cursor: pointer;
}
.info p small:hover{
  color:#ffff;
  cursor: pointer;
}
.dropdown-menu{
    min-width: auto!important;
}
#dropdownMenuButton{
    color: #fff;
    background-color: #17a2b8;
    border-color: #17a2b8;
    box-shadow: none;
}
.alert-success{ border: none; border-radius: 0; }
  [class*="sidebar-dark-"] .nav-header { border-bottom: 1px solid #4f5962; }

.select2-container--default .select2-selection--multiple .select2-selection__choice{ background-color: #007bff; }
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove{ color: #fff; }
.bg-light-grey{ background-color: #afb2b5 !important; color: #000; }
.square-alert{ border-radius: 0 !important; }
.was-validated .text_area:valid { background-image: none; border: 1px solid #ced4da !important;}
.was-validated .input_text:valid {background-image: none !important; border: 1px solid #ced4da !important;}
.was-validated .custom-select:valid { background-image: none; border: 1px solid #ced4da !important; }
  .was-validated .custom-file-input:valid { background-image: none !important; border: 1px solid #ced4da !important; }

</style>




</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
<?php
if(Auth::check() != null){
  $user = App\Models\User::where('id', Auth::user()->id)->first();
}
?>
  <!-- Preloader -->
  <!-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="4T2 Audyt Docx Logo" height="55" width="60">
  </div> -->
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ url('dashboard') }}" class="nav-link">Home</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="{{ url('dashboard') }}" class="brand-link">
      <!-- <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="4T2 Audyt Docx Logo" class="brand-image img-circle elevation-3"> -->
      <span class="brand-text font-weight-light">Contact Management</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
       <div class="user-panel mt-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">@if (Auth::check() != null){{auth()->user()->name}}@endif</a>
          <p><small>Admin</small></p>
        </div>
        </div>
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
           <li class="nav-item">
            <a href="{{ url('contacts') }}" class="nav-link">
              <i class="nav-icon fas fa-file-alt"></i>
              <p>Contacts</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('reviews-list') }}" class="nav-link">
              <i class="nav-icon fas fa-file-alt"></i>
              <p>Reviews</p>
            </a>
          </li>
         
          
          <li class="nav-header"></li>
          <li class="nav-item">
            <a class="nav-link" id="nav-logout-action">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="pointer" >Logout</p>
              <div class="d-none">
                <form id="nav-logout-form" method="POST" action="{{ url('logout') }}">
                  @csrf
                  <button type="submit">
                </form>
              </div>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper pt-2">
    @yield('content')
  </div>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021-2025 <a href="https://adminlte.io">Contact Management</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 0.2 (Beta)
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<!-- ./wrapper -->
<script type="text/javascript">// Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
      'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
  .forEach(function (form) {
    form.addEventListener('submit', function (event) {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')
    }, false)
  })
})()
</script>
</body>
</html>
