@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
       
    </div>
    
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
        </ul>
    </div>
    @endif
    @if (\Session::has('message'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        <li>{!! \Session::get('message') !!}</li>
    </ul>
</div>
@endif
    <!-- /.card-header -->
    <div class="card-body">
        <table id="review_list_grid" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Contact Name</th>
                    <th>Reviewer Name</th>
                    <th>Reviewer Email</th>
                    <th>Review</th>
                    <th>Is Student</th>
                    <th>University</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($review_data as $row)
                @if($row['review'])
                <tr>
                    <td>{{$row['review']['contact']['first_name']." ".$row['review']['contact']['last_name']}}</td>
                    <td>{{$row['first_name']." ".$row['last_name']}}</td>
                    <td>{{$row['email']}}</td>
                    <td>{{$row['review']['review']}}</td>
                    <td>{{$row['is_student']}}</td>
                    <td>{{$row['university'] }}</td>
                    <td>{{date("d-m-y H:i:s",strtotime($row['review']['created_at']))}}</td>
                </tr>
                @endif
                @endforeach
            </tbody>
            
        </table>
    </div>
    <!-- /.card-body -->
</div>

<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script>

</script>
<script>

    
    $(function () {
        $(document).ready(function () {
            // $('#review_list_grid thead tr')
            //     .clone(true)
            //     .addClass('filters')
            //     .appendTo('#review_list_grid thead');
            var table = $('#review_list_grid').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                autoWidth:false,
                columnsDefs: [ { "width": "20%" }, null],
                buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
                initComplete: function () {
                    var api = this.api();

                    // For each column
                    api
                        .columns()
                        .eq(0)
                        .each(function (colIdx) {
                            // Set the header cell to contain the input element
                            var cell = $('.filters th').eq(
                                $(api.column(colIdx).header()).index()
                            );
                            var title = $(cell).text();
                            $(cell).html('<input type="text" placeholder="' + title + '" />');

                            // On every keypress in this input
                            $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                                .off('keyup change')
                                .on('keyup change', function (e) {
                                    e.stopPropagation();

                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != ''
                                                ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                : '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();

                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                },
            }).buttons().container().appendTo('#review_list_grid_wrapper .col-md-6:eq(0)');;
            
        });
    });
</script>
@endsection

