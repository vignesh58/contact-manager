@extends('layouts.admin')

@section('content')


@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (\Session::has('message'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        <li>{!! \Session::get('message') !!}</li>
    </ul>
</div>
@endif
@if (\Session::has('error'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        <li>{!! \Session::get('error') !!}</li>
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-6">
                <h3 class="card-title"></h3>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
 
        <div class="card-body">
   <form role="form" class="needs-validation" method="POST" id="formSubmit" enctype="multipart/form-data" action="{{url('review-entry')}}" novalidate>
        @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>First Name*</label>
                        <input type="text" id="first_name" name="first_name" class="form-control input_text"
                        
                        required>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="col-md-4" >
                    <div class="form-group">
                        <label>Last Name*</label>
                        <input type="text" id="last_name" name="last_name"  class="form-control input_text"
                        
                        required/>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="col-md-4" >
                 <div class="form-group">
                    <label>Email*</label>
                    <input type="email" id="email" name="email" class="form-control input_text"

                    required>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
            </div>
            <div class="col-md-4" >
                <div class="form-group">
                    <label>University*</label>
                    <input type="text" id="university" name="university" class="form-control input_text" 
                    
                    required>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label>Review*</label>
                    <textarea  id="address" name="review" class="form-control input_text"
                    required></textarea>
                </div>
                <div class="invalid-feedback">Please fill out this field.</div>
            </div>

            <div class="col-md-4" >
                <div class="form-group">
                    <label>Is student?*</label>
                    <div>
                    <div class="form-check form-check-inline">
                        <input  type="radio" name="is_student" id="is_student1" value="yes" checked>
                        <label class="form-check-label" for="is_student1" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input  type="radio" name="is_student" id="is_student2" value="no" >
                        <label class="form-check-label" for="is_student2">
                            No
                        </label>
                    </div>
                </div>
                    
                </div>
            </div>
            
        </div>
         </div>
        <input type="hidden" name="contact_id" value="{{$encoded_contact_id}}">
        <div class="card-footer">
                <button type="submit" id="createsubmit" class="btn btn-primary">Save</button>
                <a href="/client-master" class="btn btn-danger"> Cancel</a>
    </div>
    

</form> 
 </div> 
@endsection
