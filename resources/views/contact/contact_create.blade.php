@extends('layouts.admin')


@section('content')


@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (\Session::has('message'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        <li>{!! \Session::get('message') !!}</li>
    </ul>
</div>
@endif
@if (\Session::has('error'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        <li>{!! \Session::get('error') !!}</li>
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-6">
                <h3 class="card-title">{{ $data["title"] }}</h3>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
   
       
        <div class="card-body">
             <form role="form" class="needs-validation" method="POST" id="formSubmit" enctype="multipart/form-data" action="{{ url($data['form_action'])}}" novalidate>
                 @csrf
 @if($data['action'] == 'edit') 
     <input type="hidden" name="edit_id" value="{{$contact_data['id']}}">
   @endif

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>First Name*</label>
                        <input type="text" id="first_name" name="first_name" class="form-control input_text"
                        value="@if($data['action'] == 'edit'){{$contact_data['first_name']}}  @endif"
                        required>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="col-md-4" >
                    <div class="form-group">
                        <label>Last Name*</label>
                        <input type="text" id="last_name" name="last_name"  class="form-control input_text"
                        value="@if($data['action'] == 'edit') {{$contact_data['last_name']}} @endif"
                        required/>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="col-md-4" >
                   <div class="form-group">
                    <label>Email*</label>
                    <input type="email" id="email" name="email" class="form-control input_text"
                    value="@if($data['action'] == 'edit') {{$contact_data['email']}} @endif"
                    required>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
            </div>
            <div class="col-md-4" >
                <div class="form-group">
                    <label>Phone*</label>
                    <input type="text" id="phn_no" name="phn_no" class="form-control input_text" maxlength="10" 
                    value="@if($data['action'] == 'edit') {{$contact_data['phone']}} @endif"
                    required>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Address*</label>
                    <textarea  id="address" name="address" class="form-control text_area"
                    required>@if($data['action'] == 'edit') {{$contact_data['address']}} @endif</textarea>
                <div class="invalid-feedback">Please fill out this field.</div>
                </div>
            </div>
        </div>
        </div>
        <div class="card-footer">
                <button type="submit" id="createsubmit" class="btn btn-primary">Save</button>
                <a href="/client-master" class="btn btn-danger"> Cancel</a>
        </div>
    </div>
</form>
   
 </div>  
@endsection
