@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-6">
                <h3 class="card-title"></h3>
            </div>

            <div class="col-md-6 text-right user-permission">
                <a href="{{ url('contact-create') }}" class="btn btn-info" role="button">Create new</a>
            </div>
        </div>
    </div>
    
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
        </ul>
    </div>
    @endif
    @if (\Session::has('message'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <ul>
        <li>{!! \Session::get('message') !!}</li>
    </ul>
</div>
@endif
    <!-- /.card-header -->
    <div class="card-body">
        <table id="contact_list_grid" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $row)
                <tr>
                    <td>{{$row->first_name}}</td>
                    <td>{{$row->last_name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->phone}}</td>
                    <td>{{$row->address}}</td>
                    <td>
                <a class=" Edit-dd-icon" href="{{ url('contact-edit/'.$row->id) }}"><span title="Edit" class="badge badge-warning"><i class="fas fa-edit" ></i> Edit</span></a>

            <!---a class=" delete-dd-icon" href="#" data-href="{{ url('contact-delete/'.$row->id) }}"  data-toggle="modal" data-target="#contact-list-delete"><span title="Delete" class="badge badge-danger"><i class="fas fa-trash" ></i> Delete</span></a-->
             @php
            $review_id=base64_encode($row->id);
            @endphp
            <a class=" review-dd-icon" href="{{ url('reviews/'.$review_id) }}"><span title="Edit" class="badge badge-success"><i class="fas fa-comments"></i> Review</span></a>
            </td>

                </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>
    <!-- /.card-body -->
</div>
<div class="modal fade" id="contact-list-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                Confirm Delete
            </div>
            <div class="modal-body">
                Are you sure to delete?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok" href="">Delete</a>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script>

</script>
<script>
  $('#contact-list-delete').on('show.bs.modal', function(e) { 
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
   
    
    $(function () {
        $(document).ready(function () {
            // $('#emgagement_master_grid thead tr')
            //     .clone(true)
            //     .addClass('filters')
            //     .appendTo('#contact_list_grid thead');
            var table = $('#contact_list_grid').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                autoWidth:false,
                columnsDefs: [ { "width": "20%" }, null],
                buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
                initComplete: function () {
                    var api = this.api();

                    // For each column
                    api
                        .columns()
                        .eq(0)
                        .each(function (colIdx) {
                            // Set the header cell to contain the input element
                            var cell = $('.filters th').eq(
                                $(api.column(colIdx).header()).index()
                            );
                            var title = $(cell).text();
                            $(cell).html('<input type="text" placeholder="' + title + '" />');

                            // On every keypress in this input
                            $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                                .off('keyup change')
                                .on('keyup change', function (e) {
                                    e.stopPropagation();

                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != ''
                                                ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                : '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();

                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                },
            }).buttons().container().appendTo('#contact_list_grid_wrapper .col-md-6:eq(0)');;
            
        });
    });
</script>
@endsection

