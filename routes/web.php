<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
// */


//Reviews with No auth
Route::get('/reviews/{id}', [App\Http\Controllers\ReviewController::class, 'review_form']);
Route::post('/review-entry',[App\Http\Controllers\ReviewController::class, 'review_add'])->name('review-entry');


Auth::routes();

//Contacts
Route::get('/', [App\Http\Controllers\ContactController::class, 'index']);
Route::get('/contacts', [App\Http\Controllers\ContactController::class, 'index'])->name('contact-list');
Route::get('/contact-create', [App\Http\Controllers\ContactController::class, 'contact_create']);
Route::post('/contact-entry', [App\Http\Controllers\ContactController::class, 'contact_add']);
Route::post('/contact-update', [App\Http\Controllers\ContactController::class, 'contact_update']);
Route::get('/contact-edit/{id}', [App\Http\Controllers\ContactController::class, 'contact_edit']);
Route::get('/contact-delete/{id}', [App\Http\Controllers\ContactController::class, 'contact_delete']);

// Reviews with Auth
Route::get('/reviews-list', [App\Http\Controllers\ReviewController::class, 'review_list'])->middleware("auth");






