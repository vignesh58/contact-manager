<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
           $table->increments("id");
           $table->integer("review_user_id")->unsigned()->index();
           $table->text("review");
            $table->timestamps();
            $table->softDeletes();

            //FOREIGN KEY CONSTRAINTS
           $table->foreign('review_user_id')->references('id')->on('review_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');

    }
}
