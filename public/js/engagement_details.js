$(function() {
    $('.delete-doc-icon').click(function() {
        var current_element = $(this);
        var delete_doc_link = $('#delete-doc-link').val();
        var is_checklist = 0;
        var is_procedure = 0;
        if(current_element.hasClass('checklist-doc-delete')) {
            is_checklist = 1;
        }
        if(current_element.hasClass('procedure-doc-delete')) {
            is_procedure = 1;
        }
        var modal_content = {
                                param:current_element.attr('doc_code'),
                                post_link: delete_doc_link,
                                title:'Confirm Delete',
                                question: 'Are you sure to delete?',
                                proceed_button_text: 'Delete',
                                is_checklist: is_checklist,
                                is_procedure: is_procedure
                            };

        pop_confirm_modal(modal_content);
    });
    $('#modal-dialog-confirm .modal-confirm-proceed').click(function() {
        var current_element = $(this);
        var param = current_element.attr('param');
        if($('input[name=client-id]').val()){
            var client_id = $('input[name=client-id]').val();
        }
        var link = current_element.attr('post-link');
        var is_checklist = current_element.attr('is_checklist');
        var is_procedure = current_element.attr('is_procedure');
        //alert(link);
        $.ajax({
            type: 'POST',
            url: link,
            data: {
                param:param,
                is_checklist:is_checklist,
                is_procedure:is_procedure,
                client_id:client_id,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        }).done(function() {
            current_element.closest('#modal-dialog-confirm').removeClass('show');
            location.reload(true);
        });
    });
    $('input[name="checklist-status"], input[name="checklist-document"],input[name="is_completed"]').change(function(e) {
        $(this).closest('form').find('.checklist-form-button').removeClass('disabled');
    });
    $("textarea[name='checklist-note']").on('change keyup paste', function() {
        $(this).closest('form').find('.checklist-form-button').removeClass('disabled');
    });
    $('.checklist-form-button').click(function() {
        if(!$(this).hasClass('disabled')) {
            $(this).closest('form').submit();
        }
    });
});

function pop_confirm_modal(modal_content) {
    var model_element = $('#modal-dialog-confirm');
    model_element.find('.modal-confirm-title').html(modal_content.title);
    model_element.find('.modal-confirm-question p').html(modal_content.question);
    model_element.find('.modal-confirm-proceed').html(modal_content.proceed_button_text);
    model_element.find('.modal-confirm-proceed').attr('param', modal_content.param);
    model_element.find('.modal-confirm-proceed').attr('post-link', modal_content.post_link);
    model_element.find('.modal-confirm-proceed').attr('is_checklist', modal_content.is_checklist);
    model_element.find('.modal-confirm-proceed').attr('is_procedure', modal_content.is_procedure);
    model_element.modal('show');
}
